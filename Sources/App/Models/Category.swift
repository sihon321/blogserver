//
//  Category.swift
//  App
//
//  Created by Sihoon Oh on 2018. 5. 27..
//

import Vapor
import FluentSQLite

final class Category: Codable {
    var id: UUID?
    var name: String
    
    init(name: String) {
        self.name = name
    }
}

extension Category: SQLiteUUIDModel {}
extension Category: Migration {}
extension Category: Content {}
extension Category: Parameter {}

extension Category {
    var posts: Children<Category, Post> {
        return children(\.categoryID)
    }
}
