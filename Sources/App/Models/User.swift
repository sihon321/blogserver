//
//  User.swift
//  App
//
//  Created by Sihoon Oh on 2018. 5. 19..
//

import Foundation
import FluentSQLite
import Vapor

final class User: Codable {
    var id: Int?
    var userId: String
    var password: String
    var name: String
    
    init(userId: String, password: String, name: String) {
        self.userId = userId
        self.password = password
        self.name = name
    }
    
    final class Public: Codable {
         
    }
}

extension User: SQLiteModel {}
extension User: Migration {}
extension User: Content {}
extension User: Parameter {}
