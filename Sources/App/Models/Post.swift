//
//  Post.swift
//  App
//
//  Created by Sihoon Oh on 2018. 5. 22..
//

import Foundation
import FluentSQLite
import Vapor

final class Post: Codable {
    
    var id: UUID?
    var title: String
    var subTitle: String?
    var body: String?
    var user: String
    var categoryID: Category.ID
    
    init(title: String, subTitle: String, body: String?,
         user: String, categoryID: Category.ID) {
        self.title = title
        self.subTitle = subTitle
        self.body = body
        self.user = user
        self.categoryID = categoryID
    }
}

extension Post: SQLiteUUIDModel {}
extension Post: Content {}
extension Post: Migration {}
extension Post: Parameter {}

extension Post {
    var category: Parent<Post, Category> {
        return parent(\.categoryID)
    }
}
