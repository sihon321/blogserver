//
//  UsersController.swift
//  App
//
//  Created by Sihoon Oh on 2018. 5. 22..
//

import Vapor

struct UsersController: RouteCollection {
    func boot(router: Router) throws {
        let usersRoute = router.grouped("api", "users")
        usersRoute.get(User.parameter, use: getHandler)
    }
    
    func createHandler(_ req: Request) throws -> Future<User> {
        return try req.content.decode(User.self).flatMap(to: User.self) { user in
            //let hasher = try req.make(BCryptHasher.self)
            //user.password = try hasher.make(user.password)
            return user.save(on: req)
        }
    }
    
    func getHandler(_ req: Request) throws -> Future<User> {
        return try req.parameters.next(User.self)
    }
}
