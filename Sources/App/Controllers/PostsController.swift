//
//  PostsController.swift
//  App
//
//  Created by Sihoon Oh on 2018. 5. 22..
//

import Vapor

struct PostsController: RouteCollection {
    func boot(router: Router) throws {
        let postsRoute = router.grouped("api", "posts")
        postsRoute.get(use: getAllHandler)
        postsRoute.get(Post.parameter, use: getHandler)
        postsRoute.post(use: createHandler)
    }
    
    func getAllHandler(_ req: Request) throws -> Future<[Post]> {
        return Post.query(on: req).all()
    }
    
    func getHandler(_ req: Request) throws -> Future<Post> {
        return try req.parameters.next(Post.self)
    }
    
    func createHandler(_ req: Request) throws -> Future<Post> {
        let post = try req.content.decode(Post.self)
        return post.save(on: req)
    }
}
