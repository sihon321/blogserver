import Vapor
import Leaf

struct WebsiteController: RouteCollection {
    func boot(router: Router) throws {
        router.get(use: indexHandler)
        router.get("categories",use: categoryHandler)
        router.get("posts", Post.parameter, use: postHandler)
        router.get("about", use: aboutHandler)
        router.get("contact", use: contactHandler)
        router.get("postEdit", use: postEditHandler)
        
    }
    
    func indexHandler(_ req: Request) throws -> Future<View> {
        return Post.query(on: req).all().flatMap(to: View.self) { posts in
            let context = IndexContent(title: "Homepage", posts: posts.isEmpty ? nil : posts)
            return try req.leaf().render("index", context)
        }
    }
    
    func categoryHandler(_ req: Request) throws -> Future<View> {
        return Category.query(on: req).all().flatMap(to: View.self) { categories in
            let context = CategoryContext(category: categories.isEmpty ? nil : categories)
            return try req.leaf().render("category", context)
        }
    }
    
    func postHandler(_ req: Request) throws -> Future<View> {
        return try req.parameters.next(Post.self).flatMap(to: View.self) { post in
            return try post.category.get(on: req).flatMap(to: View.self) { category in
                let context = PostsContext(title: post.title, post: post, category: category)
                return try req.leaf().render("post", context)
            }
        }
    }
    
    func aboutHandler(_ req: Request) throws -> Future<View> {
        let context = EmptyContext()
        return try req.leaf().render("about", context)
    }
    
    func contactHandler(_ req: Request) throws -> Future<View> {
        let context = EmptyContext()
        return try req.leaf().render("contact", context)
    }
    
    func postEditHandler(_ req: Request) throws -> Future<View> {
        let context = EmptyContext()
        return try req.leaf().render("postEdit", context)
    }
}

extension Request {
    func leaf() throws -> LeafRenderer {
        return try self.make(LeafRenderer.self)
    }
}

struct EmptyContext: Encodable {
    
}

struct IndexContent: Encodable {
    let title: String
    let posts: [Post]?
}

struct CategoryContext: Encodable {
    let category: [Category]?
}

struct PostsContext: Encodable {
    let title: String
    let post: Post
    let category: Category
}
